##Bitmex connectors
This repository contains simple connectors to **BitMEX** exchange and set of test for them.

For this project you need to Python 3.5+ be installed. You can download it [here](https://www.python.org/downloads/).
 
 
In order to try, first you need to clone repo:
 
 `git clone https://bitbucket.org/nikita_tsykunov/system-quality.git`
 
Next move to directory, where you've just clonned repo, and run `pip` to install all requirements:

    cd system-quality
    pip install -r requirements.txt
    
After this you can run test and check that all test passed

    python main.py

Make sure, that `python.exe` in your `PATH`